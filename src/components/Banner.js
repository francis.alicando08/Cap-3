import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function Banner(){
	return(
		<Row>
		    <Col className="p-5">
		        <h1>Welcome to BNKTech!</h1>
		        <p>Your Imagination is the limit.</p>
		        <Button as = {Link} to = '/products' variant = "primary">Shop now!</Button>
		    </Col>
		</Row>
	);
}